
Run this command from the directory in which the application is deployed.

* composer install

* Ensure `logs/` is web writeable.

To run the application in development, you can also run this command. 

* composer start


To configure the enviroment, you must create the file .env in the root directory, the available variables are:

* DB_HOST

* DB_PORT

* DB_NAME

* DB_USER

* DB_PASS

* DB_CHARSET

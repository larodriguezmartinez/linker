<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Controllers\BaseController;
class RepairTypeController extends BaseController{

    public function getAll( $request, $response, $arg){

        $pdo = $this->container->get('db');

        $query = $pdo->query("SELECT * FROM repair_types");

        $payload = json_encode($query->fetchAll());

        $response->getBody()->write($payload);

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);

    }

}
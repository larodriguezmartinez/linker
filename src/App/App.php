<?php

use Slim\Factory\AppFactory;

require __DIR__ . '/../../vendor/autoload.php';

$initContainer = new \DI\Container();

AppFactory::setContainer($initContainer);

$app = AppFactory::create();

$container = $app->getContainer();

require __DIR__ . '/Routes.php';

require __DIR__ . '/Config.php';

require __DIR__ . '/Dependencies.php';

$app->run();
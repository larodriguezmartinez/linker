<?php

use Slim\Routing\RouteCollectorProxy;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


$app->get('/', function (Request $request, Response $response, $args) {

    $response->getBody()->write('<h1>Linker</h1>');    

    return $response;

});

$app->group('/api/v1', function( RouteCollectorProxy $group){

    $group->get('/repair-types', 'App\Controllers\RepairTypeController:getAll');

});
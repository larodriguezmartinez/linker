<?php

use Psr\Container\ContainerInterface;

$container->set('db', function(ContainerInterface $c){

    $config = $c->get('db_settings');

    $config->DB_CHARSET;

    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
    ];

    $dsn = "mysql:host=".$config->DB_HOST.";dbname=".$config->DB_NAME.";charset=".$config->DB_CHARSET.";port=".$config->DB_PORT."',";

    return new PDO($dsn, $config->DB_USER, $config->DB_PASS, $options );

});
<?php

$dotenv = Dotenv\Dotenv::createImmutable('../');

$dotenv->load();

$container->set('db_settings', function(){
    return (object)[
        "DB_HOST" => $_ENV['DB_HOST'],
        "DB_USER" => $_ENV['DB_USER'],
        "DB_PASS" => $_ENV['DB_PASS'],
        "DB_NAME" => $_ENV['DB_NAME'],
        "DB_CHARSET" => $_ENV['DB_CHARSET'],
        "DB_PORT" => $_ENV['DB_PORT']
    ];
});